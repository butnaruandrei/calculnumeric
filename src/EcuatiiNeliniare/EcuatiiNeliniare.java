package EcuatiiNeliniare;

import java.util.ArrayList;

/**
 * Created by ButnaruAndrei on 2/27/14.
 */
public abstract class EcuatiiNeliniare {
    double prec;

    public EcuatiiNeliniare() {
        prec = Math.pow(10, -9);
    }

    public double metoda_bisectiei_siruri(double _a, double _b) {
        ArrayList<Double> a = new ArrayList<>();
        ArrayList<Double> b = new ArrayList<>();
        ArrayList<Double> c = new ArrayList<>();
        double t;
        int m = 0;

        a.add(0, _a);
        b.add(0, _b);
        c.add(0, (_a + _b) / 2);
        do {
            m++;
            t = f(c.get(m - 1));
            if (t == 0d) {
                a.add(a.get(m - 1));
                b.add(b.get(m - 1));
                c.add(c.get(m - 1));
            } else if (f(a.get(m - 1)) * t < 0d) {
                a.add(a.get(m - 1));
                b.add(c.get(m - 1));
                c.add((a.get(m) + b.get(m)) / 2);
            } else {
                a.add(c.get(m - 1));
                b.add(b.get(m - 1));
                c.add((a.get(m) + b.get(m)) / 2);
            }
        } while (!(Math.abs(f(c.get(m))) < prec && Math.abs(c.get(m) - c.get(m - 1)) < prec));

        //System.out.println("Radacina se gaseste in intervalul [" + Math.floor(c.get(m)) + ", " + Math.ceil(c.get(m)) + "]. Solutia ecuatiei este: " + c.get(m) + " si a fost gasita in " + m + " iteratii.");
        System.out.format("Radacina se gaseste in intervalul [%d, %d]. Soluatia ecuatiei este %10.10f si a fost gasita in %d iteratii", (int)Math.floor(c.get(m)), (int)Math.ceil(c.get(m)), c.get(m), m);
        return c.get(m);
    }

    public double regula_falsi_siruri(double _a, double _b) {
        ArrayList<Double> a = new ArrayList<>();
        ArrayList<Double> b = new ArrayList<>();
        ArrayList<Double> c = new ArrayList<>();
        double t, t_a, t_b;
        int m = 0;

        a.add(0, _a);
        b.add(0, _b);
        t_a = f(a.get(0));
        t_b = f(b.get(0));
        c.add(0, (a.get(0) * t_b - b.get(0) * t_a) / (t_b - t_a));
        do {
            m++;
            t = f(c.get(m - 1));
            if (t == 0d) {
                a.add(a.get(m - 1));
                b.add(b.get(m - 1));
                c.add(c.get(m - 1));
            } else if (f(a.get(m - 1)) * t < 0d) {
                a.add(a.get(m - 1));
                b.add(c.get(m - 1));
                t_a = f(a.get(m));
                t_b = f(b.get(m));
                c.add((a.get(m) * t_b - b.get(m) * t_a) / (t_b - t_a));
            } else {
                a.add(c.get(m - 1));
                b.add(b.get(m - 1));
                t_a = f(a.get(m));
                t_b = f(b.get(m));
                c.add((a.get(m) * t_b - b.get(m) * t_a) / (t_b - t_a));
            }
        } while (!(Math.abs(f(c.get(m - 1))) < prec && Math.abs(c.get(m) - c.get(m - 1)) < prec));

        //System.out.println("Radacina se gaseste in intervalul [" + Math.floor(c.get(m)) + ", " + Math.ceil(c.get(m)) + "]. Solutia ecuatiei este: " + c.get(m) + " si a fost gasita in " + m + " iteratii.");
        System.out.format("Radacina se gaseste in intervalul [%d, %d]. Soluatia ecuatiei este %10.10f si a fost gasita in %d iteratii", (int)Math.floor(c.get(m)), (int)Math.ceil(c.get(m)), c.get(m), m);
        return c.get(m);
    }

    public double metoda_coardei_siruri(double _a, double _b) {
        ArrayList<Double> x = new ArrayList<>();
        int m = 0;
        double t_x0, t_xn;
        x.add(_a);
        x.add(_b);

        t_x0 = f(x.get(0));
        do {
            m++;
            t_xn = f(x.get(m));
            x.add((x.get(0) * t_xn - x.get(m) * t_x0) / (t_xn - t_x0));
        } while (!(Math.abs(f(x.get(m))) < prec && Math.abs(x.get(m) - x.get(m - 1)) < prec));

        //System.out.println("Radacina se gaseste in intervalul [" + Math.floor(x.get(m)) + ", " + Math.ceil(x.get(m)) + "]. Solutia ecuatiei este: " + x.get(m) + " si a fost gasita in " + m + " iteratii.");
        System.out.format("Radacina se gaseste in intervalul [%d, %d]. Soluatia ecuatiei este %10.10f si a fost gasita in %d iteratii", (int)Math.floor(x.get(m)), (int)Math.ceil(x.get(m)), x.get(m), m);
        return x.get(m);
    }

    public double metoda_secantei_siruri(double _a, double _b) {
        ArrayList<Double> x = new ArrayList<>();
        int m = 0;
        double t_x0, t_x1;
        x.add(_a);
        x.add(_b);

        do {
            m++;
            t_x0 = f(x.get(m - 1));
            t_x1 = f(x.get(m));
            x.add((x.get(m - 1) * t_x1 - x.get(m) * t_x0) / (t_x1 - t_x0));
        } while (!(Math.abs(f(x.get(m))) < prec && Math.abs(x.get(m) - x.get(m - 1)) < prec));

        //System.out.println("Radacina se gaseste in intervalul [" + Math.floor(x.get(m)) + ", " + Math.ceil(x.get(m)) + "]. Solutia ecuatiei este: " + x.get(m) + " si a fost gasita in " + m + " iteratii.");
        System.out.format("Radacina se gaseste in intervalul [%d, %d]. Soluatia ecuatiei este %10.10f si a fost gasita in %d iteratii", (int)Math.floor(x.get(m)), (int)Math.ceil(x.get(m)), x.get(m), m);
        return x.get(m);
    }

    public double metoda_newton_siruri(double _a, double _b) {
        ArrayList<Double> x = new ArrayList<>();
        int m = 0;
        x.add(_a);

        do {
            m++;
            x.add(x.get(m-1) - f(x.get(m-1))/fd(x.get(m-1)));
        } while (!(Math.abs(f(x.get(m))) < prec && Math.abs(x.get(m) - x.get(m - 1)) < prec));
        //System.out.println("Radacina se gaseste in intervalul [" + Math.floor(x.get(m)) + ", " + Math.ceil(x.get(m)) + "]. Solutia ecuatiei este: " + x.get(m) + " si a fost gasita in " + m + " iteratii.");
        System.out.format("Radacina se gaseste in intervalul [%d, %d]. Soluatia ecuatiei este %10.10f si a fost gasita in %d iteratii", (int)Math.floor(x.get(m)), (int)Math.ceil(x.get(m)), x.get(m), m);
        return x.get(m);
    }

    private double fd(double x){
        double h = x * 1e-8;
        if (h == 0) {
            return -1;
        } else {
            return (f(x + h) - f(x - h)) / (2 * h);
        }

    }

    abstract double f(double x);
}
