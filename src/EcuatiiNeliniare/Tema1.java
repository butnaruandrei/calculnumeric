package EcuatiiNeliniare;

/**
 * Created by ButnaruAndrei on 2/27/14.
 */
public class Tema1 extends EcuatiiNeliniare {

    public void executa() {
        System.out.println("Metoda bisectiei: ");
        double rezultat = metoda_bisectiei_siruri(-10, 10);

        System.out.println("\nRegula falsi");
        rezultat = regula_falsi_siruri(-2, -1);

        System.out.println("\nMetoda coardei");
        rezultat = metoda_coardei_siruri(-10,10);

        System.out.println("\nMetoda secantei");
        rezultat = metoda_secantei_siruri(-10,10);

        System.out.println("\nMetoda lui Newton");
        rezultat = metoda_newton_siruri(-10,10);
    }

    @Override
    double f(double x) {
        return Math.pow(x, 5) + 8 * Math.pow(x, 4) + 17 * Math.pow(x, 3) - 8 * Math.pow(x, 2) - 14 * x + 20;
    }
}
